import * as React from 'react';
import * as ReactDOM from 'react-dom';
import registerServiceWorker from './registerServiceWorker';

import { I18nextProvider } from 'react-i18next';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import { applyMiddleware, compose, createStore } from 'redux';
import thunk from 'redux-thunk';

import ErrorPreLoadingPage from 'src/common/components/error-pre-loading';
import App from 'src/common/containers/app';
import i18n from 'src/common/lib/i18n';
import { Session } from 'src/common/lib/session';
import { User } from 'src/common/redux/user/state';
import { moduleAllowsAnonymous, moduleName } from 'src/routes';

function page(user: User) {
    //Load bootstrap now (not before)
    require('bootstrap');

    //See https://github.com/zalmoxisus/redux-devtools-extension
    //Really useful to make applications with redux inside (only for chrome and firefox)
    const composeUpdated = window['__REDUX_DEVTOOLS_EXTENSION_COMPOSE__'] || compose;
    const store = createStore(
        require('./redux').reducers,            ///< Load now the reducers, not before
        { user },                               ///< Initial state
        composeUpdated(applyMiddleware(thunk))  ///< Apply the widdleware and the extension
    );

    //As we cannot comment JSX code, it will be commented before
    //<Provider/> is for making available redux in the whole React app (see /src/common/redux/README.md)
    //<BrowserRouter/> is for applying the router in the whole app (see https://reacttraining.com/react-router/web/guides/philosophy )
    //<I18nextProvider/> is for making available in the whole react app the translations services (see https://www.i18next.com and https://react.i18next.com )
    //<App/> is our main entry point (DO NOT CHANGE UNLESS YOU KNOW WHAT ARE YOU DOING) See its documentation :)
    ReactDOM.render(
        <Provider store={store}>
            <BrowserRouter basename={ `/${moduleName}` }>
                <I18nextProvider i18n={ i18n }>
                    <App />
                </I18nextProvider>
            </BrowserRouter>
        </Provider>,
        document.getElementById('root') as HTMLElement
    );

    //A service worker to add to cache the app. You can disable if you want to opt-out
    //See https://github.com/facebook/create-react-app/blob/master/packages/react-scripts/template/README.md#making-a-progressive-web-app
    registerServiceWorker();
}

//Yep, do it async \m/
(async () => {
    try {
        if(process.env.NODE_ENV !== 'production') {
            //Redirects to the correct path (only in debug)
            if(!window.location.pathname.startsWith(`/${moduleName}`)) {
                window.location.assign(`/${moduleName}`);
            }
        }

        //Loads the user from the session and converts to the one that will be used in redux
        //If there's any problem with the process of checking the session, it will throw an error
        //If the error is null (weird design from Melchor), it means that you must call Session.logIn() NOW
        const user = Session.convertToStatusUser(await Session.checkSession(moduleAllowsAnonymous), moduleName);

        page(user);
    } catch(e) {
        //If we receive this, it means that there's no user authenticated (or invalid) and then...
        if(e === null) {
            if(moduleAllowsAnonymous) {
                //...it will login as guest
                page(Session.anonymousUser(moduleName));
            } else {
                //...we must proceed to log in
                Session.logIn();
            }
            return;
        }

        //Show an beautiful error, and a page too
        console.error(e);
        ReactDOM.render(
            <ErrorPreLoadingPage error={ e } />,
            document.getElementById('root') as HTMLElement
        );
    }
})();
