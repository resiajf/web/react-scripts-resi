import asyncComponent from 'src/common/components/async-component';
import { Route, route } from 'src/common/lib/utils';

//The module name.
// [!!] Must match with the module name defined in the permissions to allow the pages to show.
export const moduleName = 'hello'; //TODO Change the module name

//Allows anonymous/guests users in this module (avoids redirect to login if there's no user)
export const moduleAllowsAnonymous = false;

//A list of pages. Try to use async components to reduce the bloat in the main js.
//These async pages loads when the page is accessed (not before).
//See the documentation of asyncComponent.
// [!!] Remember that a component loaded like this must, the component must be exported using `export default ...`.
const Hello = asyncComponent(() => import('./components/hello'));

//The list of routes of your module. Put here all the routes, with the components.
//  1. The first argument is the path of the route. You can receive some nice parameters
//     with '/:myParam', and adding in the component props the type
//     RouteComponentProps<{ myParam: string }>. You can make params optional with the
//     question mark: '/:page?'. But also, you can add simple regular expressions to the
//     parameters: '/:page(\\d+)?' (notice the two back slashes here).
//     See https://github.com/pillarjs/path-to-regexp/ for more.
//
//  2. The second argument is the name of the permission that relates to this page.
//     This allows you to hide a group of routes if a user is not allowed to acces it
//     by simply not adding the permission in their permission list of its role.
//     You can, as mentioned, group a list of routes under the same route, and also
//     some of them can be hidden from the sidebar (see point 5).
//
//  3. The component to render when the route matches.
//
//  4. A page identifier. The pages must have different identifiers to allow you to
//     differenciate them, and also allows translator to translate the name of the
//     page for the sidebar list.
//
//  5.  Extra properties (optional). The optional values available are:
//        - exact: If set to true, it will match only the path, if not set or set to
//          false, it will match the route as a prefix
//        - hide: Hides the route from the sidebar. Use it when grouping a list of
//          routes with the same permission key (as seen in 2.). It is useful when
//          implementing pagination over the routes (the path holds the page the
//          user is) that in general needs some different paths to make it work good.
//
//If you see that a route is not working, sometimes is because the path is not well
//defined (test them in https://npm.runkit.com/path-to-regexp) or because of the order
//of the routes.
//To make a route redirect to another, you can use Redirect component from react-router-dom.
export const routes: Array<Route<any, any>> = [
    route('/', 'hello', Hello, 'hello', { exact: true }),
];