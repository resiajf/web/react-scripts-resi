import { combineReducers } from 'redux';

import { reducers as commonReducers, State as CommonState } from 'src/common/redux';

//Combine the common reducers (from the common package) and your reducers here
export const reducers = combineReducers<State>({
    ...commonReducers,
    //TODO Put some reducers here
});

//Put here your state types to match the combineReducers State
// tslint:disable-next-line:no-empty-interface
export interface State extends CommonState {
    //TODO Put some state types here
}
