import React from 'react';

import imatge from './imatge.jpg'; //This way, you can load pages, imatge will have the route to it

//A nice hello page
// [!!] Take note that the component is using `export default` as it is being loaded using asyncComponent
export default () => (
    <div className="text-center">
        <img className="img-fluid" src={imatge} />
    </div>
);