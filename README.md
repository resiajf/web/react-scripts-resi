# `react-scripts-resi`

Create React apps (with the template for the residence) with no build configuration.

 * [Getting Started](#tldr) – How to create a new app.
 * [User Guide](https://github.com/wmonk/create-react-app-typescript/blob/master/template/README.md) – How to develop apps bootstrapped with react scripts ts.
 * [TypeScript upstream repository](https://github.com/wmonk/create-react-app-typescript) - See for special changes in the new versions
 * [What is this package installing and what's for](#what-is-this-package-installing-and-whats-for)
 * [What you should know from the installed packages](#what-you-should-know-from-the-above-packages)
   * [React](#react)
   * [TypeScript](#typescript)
   * [Bootstrap](#bootstrap)
   * [moment.js](#momentjs)
   * [react-i18n](#react-i18n)
   * [redux and redux-thunk](#redux-and-redux-thunk)
   * [react-redux](#react-redux)
   * [react-router](#react-router)
   * [react-toastify](#react-toastify)
   * [SCSS](#scss)
 * [What is the arquitecture of the generated react app](#what-is-the-arquitecture-of-the-generated-react-app)

## tl;dr

```sh
npx create-react-app app-test --scripts-version=https://gitlab.com/resiajf/web/react-scripts-resi.git
cd app-set
git init
git submodule add https://gitlab.com/resiajf/web/app-common-content.git src/common
```

**It is recommended** to have installed and use [`yarn`](https://yarnpkg.com) instead of `npm`.

## What is this package installing and what's for

Basic packages (from `create-react-app-ts`):

 - [react][1]: the library to make the interfaces (abstract)
 - [react-dom][2]: the implementation of [react][1] for the browser
 - react-scripts-resi: this package :)
 - [typescript][3]: the compiler of TypeScript, allows you to use TS for development

Extra packages:

 - [bootstrap][4]: style framework for the web, used as base for styling the app
 - [hamburgers][5]: nice hamburger animation, for the hide/show button for the sidebar, located in the blue header (you'll see soon)
 - [i18next][6]: localization library
 - [i18next-browser-languagedetector][7]: detector for i18next that detects the language(s) from the browser properties and local storage
 - [i18next-xhr-backend][8]: backend for loading translations that loads `json`s from a path
 - [jquery][9]: a JavaScript library used by bootstrap (you can use it as well, but try to avoid it)
 - [moment][10]: library for dates and times in JavaScript, helps you show dates and times in the current locale, and manipulate them easily
 - [popper.js][11]: dependency for bootstrap
 - [react-helmet][12]: easily change elements from the `<head></head>` section of the document inside react (you can change the title here)
 - [react-i18next][13]: react bindings for [i18next][6]
 - [react-redux][14]: react bindings and utilities for [redux][19]
 - [react-router][15]: react library for enabling routing inside an app (abstract)
 - [react-router-dom][16]: implementation of [react-router][15] for the browser
 - [react-spring][17]: allows you to make sweet animations (not used by default, but you can use whenever you want)
 - [react-toastify][18]: send nice notifications in-browser to the user
 - [redux][19]: predictable state container for JavaScript apps
 - [redux-thunk][20]: middleware for [redux][19] that allows asynchronous dispatch of actions

Extra development packages:

 - [npm-run-all][21]: run a bunch of `scripts` from a `package.json`, allows to run the development server and build the `.scss` files
 - [node-sass-chokidar][22]: better compiler for [SASS/SCSS][23], used for compile `.scss` files into `.css` files.

## What you should know from the above packages

### React

How to build components in `react`. You should first try [their tutorial][24] and then [their getting started][25].

For TypeScript, you should now that `React.Component` and `React.PureComponent` accept two types parameters: the first for the type of the properties and the second for the type of the state. Here's an example of a component with full typings.

```tsx
import * as React from 'react'; //in our configuration `import React from 'react';` is also valid (and recommended)

//Define the properties
//You should see `/containers/README.md` and `/redux/README.md` from the common package
//to know that the below interface is not needed :)
interface ComponentProperties {
    actions: number;
    onIncrementClicked?: (value?: number) => void;
    onDecrementClicked?: (value?: number) => void;
}

interface ComponentState {
    howMuchToIncrement: number;
}

export class ExampleComponent extends React.Component<ComponentProperties, ComponentState> {

    constructor(props: ComponentProperties) {
        super(props);

        //Initialize the state
        this.state = {
            howMuchToIncrement: 1,
        };

        //JavaScript obscure behaviours...
        //If you intend to run a a method of the class passing it directly into a property, you must do this
        this.onDecrementClicked = this.onDecrementClicked.bind(this);
        this.onIncrementClicked = this.onIncrementClicked.bind(this);
        this.howMuchChanged = this.howMuchChanged.bind(this);
    }

    public render() {
        //is the same as `const actions = this.props.actions;`
        const { actions } = this.props;
        const { howMuchToIncrement } = this.state;

        //<> is a fragment, is like a <div> but without generating an element in the DOM tree https://reactjs.org/docs/fragments.html
        //See https://reactjs.org/docs/forms.html to understand the behaviour of the input
        return (
            <>
                <code>You've done { actions } action{ actions === 1 ? '' : 's' }</code>
                <button onClick={ this.onDecrementClicked }>↓</button>
                <input type="number" min={ 1 } step={ 1 } value={ howMuchToIncrement } onChange={ this.howMuchChanged } />
                <button onClick={ this.onIncrementClicked }>↑</button>
            </>
        );
    }

    private howMuchChanged(e: React.ChangeEvent<HTMLInputElement>) {
        //Remember: state is immutable, you must notify what you want to change for which value
        this.setState({
            //The max is not needed, but for showing you how to implement some restrictions in the listener
            howMuchToIncrement: Math.max(1, Number(e.target.value)),
        });
    }

    private onDecrementClicked(e: React.MouseEvent<HTMLButtonElement>) {
        e.preventDefault();
        //If values are nullable (or can be undefined), inside this if will make them non-optional
        if(this.props.onDecrementClicked) {
            this.props.onDecrementClicked(this.state.howMuchToIncrement);
        }
    }

    private onIncrementClicked(e: React.MouseEvent<HTMLButtonElement>) {
        e.preventDefault();
        if(this.props.onIncrementClicked) {
            this.props.onIncrementClicked(this.state.howMuchToIncrement);
        }
    }

}
```

### TypeScript

You should learn how to code in [TypeScript][3]. Is not difficult, just remember to put the type of things and define interfaces and types all the time. You've already seen some TS in the above example.

### Bootstrap

In general, they are just `div`s and classes (`className`s in React, don't forget) that used correctly, makes incredible things.

It is recommended to check the [grid][26] system (you'll be using it almost at all times), [typography][27], [tables][28] and remember [utilities][29] always. In general, you should see what's available in bootstrap to simplify your life.

### moment.js

When dealing with dates and times, utilities like [moment.js][10] avoids you to wanting to kill yourself. Use it wisely, and remember that also shows times in different laguages.

### react-i18n

Remember to always translate your strings. A quick guide is:

```tsx
import React from 'react';
import { InjectedTranslateProps, Trans, translate } from 'react-i18next';

class ComponentClass extends React.Component<InjectedTranslateProps> {

    public render() {
        //If you need to simply show a text, you can use the prop `t`
        //If you need more complex things like elements inside the text (like the second)
        //  example, then the element Trans could help you go through. The translation indices
        //  are a bit weird, but they make sense. See the documentation, don't forget!
        return (
            <>
                <p>{ this.props.t('key') }</p>
                <p><Trans i18nKey="another.key">some text that <b>won't show</b> if it is available in translations <code>{{ value:3 }}</code>{{ text: 'nothings' }}</Trans></p>
            </>
        )
    }

}

export default translate()(ComponentClass);
```

And in the json of translations, you will have:

```json
{
  "key": "Here you have the first translation",
  "another": {
    "key": "That text <1>will show</1> because it is the translation yes <3><0>{{value}}</0></3><4>{{text}}</4>",
   }
}
```

### redux and redux-thunk

For [redux][19] and [redux-thunk][20], here it is a quick guide.

This common redux states and reducers allows you to have some common behaviour. But first, what is [redux][19]?

 > Redux is a predictable state container for JavaScript apps.
 >
 > It helps you write applications that behave consistently, run in different environments (client, server, and native), and are easy to test.

What makes redux special? Well it keeps some logic of applications separated from the UI logic. That is, logic like loading data from server, doing modifications to it and so should be handled in this place, as it is common for the whole app.

You should read a bit about redux before starting:

 - [Introduction][30]
 - [Glossary][31]
 - [Basics][32]
 - [Advanced][33]

Redux modifies the state using `actions`. These actions are JS objects that have a `type` of the action and any data you need. With [redux-thunk][20] you can make asynchronous requests by returning a function with one parameter (`dispatch` to dispatch more actions).

```js
export const MY_ACTION = 'MY_ACTION';
export const LOADING_POSTS = 'LOADING_POSTS';
export const LOADED_POSTS = 'LOADED_POSTS';
export const ERROR_LOADING_POSTS = 'ERROR_LOADING_POSTS';

//Common action in redux
export const anAction = (op, value = 1) => ({
    type: MY_ACTION,
    op,
    value,
});

//That will be handled by redux-thunk
export const anAsyncAction = (page) => (dispatch) => {
    dispatch({ type: LOADING_POSTS, page });
    fetch(`/my/api/posts?page=${page + 1}`)
        .then(res => {
            if(res.ok) {
                return res.json();
            } else {
                res.json().then(error => dispatch({ type: ERROR_LOADING_POSTS, error, page }));
            }
        })
        .then(posts => dispatch({ type: LOADED_POSTS, posts, page }))
        .catch((error) => dispatch({ type: ERROR_LOADING_POSTS, error, page }));
};
```

```typescript
//And that's how you define action types in TypeScript, useful for typings in the reducer
import { Action } from 'redux';

export interface AnActionAction extends Action<'MY_ACTION'> { //Well, it's better to use `typeof MY_ACTION`
    op: 'increment' | 'decrement';
    value: number;
    //type is not needed, is inside Action
}

export interface LoadingPostsAction extends Action<'LOADING_POSTS'> {
    page: number;
}

export interface ErrorLoadingPostsAction extends Action<'ERROR_LOADING_POSTS'> {
    error: any;
    page: number;
}

export interface LoadedPostsAction extends Action<typeof LOADED_POSTS> { //This should be the right way
    posts: Post[];
    page: number;
}

export type MyReducerActions = AnActionAction | LoadingPostsAction | ErrorLoadingPostsAction | LoadedPostsAction;
```

When an action is dispatched, it will call all the reducers to modify their state. The reducers have its onw state and they are only able to modify its state. Other reducer's state cannot be modified in a reducer. A reducer takes an action and does something if the action has to modify its state, if not, simply returns the same state. The most important thing about [redux][1] is that the **state is immutable**, you must not modify the state directly, instead return copies of it.

```js
import { MY_ACTION, LOADING_POSTS, ERROR_LOADING_POSTS, LOADED_POSTS } from './actions';

const myReducer = (state, action) => {
    switch(action.type) {
        //If the type of the action is one of our actions, do something
        case MY_ACTION: {
            return {
                 //This is called spread operator, copies the object with all its content to a new object...
                ...state,
                //...and then you can override some of the properties (at the end, is a new object)
                actions: action.op === 'increase' ? state.actions + action.value : (action.op === 'decrease' ? state.actions - action.value : state.actions),
            };
        }

        case LOADING_POSTS: {
            return {
                ...state,
                posts: {
                    ...state.posts,
                    loading: true,
                    page: action.page,
                }
            };
        }

        case LOADED_POSTS: {
            return {
                ...state,
                posts: {
                    ...state.posts,
                    loading: false,
                    page: action.page,
                    //The type of this `pages` is Post[][], where the first indicates the page and the second the
                    //element inside the page. The code copies all the pages before the loaded modifies the loaded page
                    //and then copies the pages after the loaded.
                    pages: [
                        ...state.posts.pages.slice(0, action.page), //Spread operator also works for arrays :)
                        action.posts,
                        ...state.posts.pages.slice(action.page + 1), //Slice is like list[2:3] in R or Python
                    ],
                }
            };
        }

        case ERROR_LOADING_POSTS: {
            return {
                ...state,
                posts: {
                    ...state.posts,
                    loading: false,
                    page: action.page,
                    pages: [
                        ...state.posts.pages.slice(0, action.page),
                        null, //Indicates an error
                        ...state.posts.pages.slice(action.page + 1),
                    ],
                    error: action.error, //This should be different, but is an example :)
                }
            };
        }

        //If the action does not fit inside our reducer (doesn't need to be handled), just return the state untouched...
        default: {
            //... except if the state is the default one (undefined). In that case, you must define the initial state.
            return state || {
                actions: 0,
                posts: {
                    loading: false,
                    page: null,
                    pages: [],
                    error: null,
                },
            };
        }
    }
};
```

For redux, it can only be one reducer. So, to combine all our reducers, we must use `combineReducers(reducers: object)` function. This will create a big reducer with our little reducers. For this, you should assign a name for your reducer and the reducer to it.

For the project, as this is using TypeScript, you should define an interface with the state of your reducer, as you can see in `/redux/errors/state.ts` and (optionally) define the types of the actions as seen in `/redux/errors/actions.ts` (both files from the common package).

```typescript
import { combineReducers } from 'redux';
import myReducer from './myReducer';

export const app = combineReducers({
    myReducer, //this is the same as `myReducer: myReducer`
});
//The type of the state now is:
type State = {
    //You should define a separated interface for every reducer's state
    myReducer: {
        actions: number;
        posts: {
            loading: boolean;
            page: number | null; //Number or null (it's typescript :)
            pages: Array<Array<Post> | null>; //An array of type (array of posts or null)
            error: any | null; //Any is any type now
        };
    };
}
```

At the end, you must create a `store` to handle the state, call the reducers and dispatch actions. Also, when creating the store, you can apply middlewares like [redux-thunk][6].

```js
import { applyMiddleware, createStore } from 'redux';
import thunk from 'redux-thunk';
import { app } from './redux';

const store = createStore(app, applyMiddleware(thunk));
const store = createStore(app, window.INITIAL_STATE_FROM_ELSEWHERE, applyMiddleware(thunk));
```

To make it work with react, simply add the store provider in the `render()` function before any other component.

```jsx harmony
import { render } from 'react-dom';
import { Provider } from 'react-redux';

import App from './app';

//...

render(
    <Provider store={ store }>
        <App />
    </Provider>,
    document.getElementById('root')
);
```

See more information on this below.

### react-redux

For [react-redux][14], here it is another quick guide in the common package. But you should read about redux before.

When mixing redux with react, a new concept appears: Container Components. These components are made by [redux-react][4] and require you to define how to bind a _Presentational Component_ (a common component on React) with the redux state. For this, you can define `mapStateToProps` and (optionally) `mapDispatchToProps` to allow the component to modify the redux state by applying actions and joining this with [`connect`][34] function.

```typescript
import { connect } from 'react-redux';
import { anAction } from '../redux/actions';
import { State } from '../redux';
import { ExampleComponent } from '../components/example'; //See README.md from `create-react-app-resi`

//These interfaces should be in another file (big recommendation)
interface ComponentStateToProps {
    actions: number;
}

interface ComponentDispatchToProps {
    onIncrementClicked: (value?: number) => void;
    onDecrementClicked: (value?: number) => void;
}

export type ComponentProps = ComponentStateToProps & ComponentDispatchToProps;

//Defines how to connect the redux state with the props of the component
const mapStateToProps = ({ myReducer }: State): ComponentStateToProps => ({
    actions: myReducer.actions,
});

//Defines some functions as props that allows the app to modify the redux state
const mapDispatchToProps = (dispatch: any): ComponentDispatchToProps => ({
    onIncrementClicked: (value) => dispatch(anAction('increment', value)),
    onDecrementClicked: (value) => dispatch(anAction('decrement', value)),
});

//Make the "container component" by connecting the mapStateToProps and mapDispatchToProps functions with a "presentational component"
export const Component = connect(mapStateToProps, mapDispatchToProps)(PresentationalComponent);
```

 > **Note**: If you need to make a page and you are using asyncComponent decorator, you must use `export default` instead of `expor const Component =`;

 > **Another note**: If you need to access to the route information from [react-router][35] and is not working, try to use the function `withRouter()` with the result of the `connect` function: `withRouter(connect(mapStateToProps, mapDispatchToProps)(PresentationalComponent))`.

That's how you make a _Container Component_ with the state of redux. Look for the app modules, you can take them as a good examples of containers.

### react-router

For [react-router][15], there's a big comment in `/template/src/routes.tsx` that could help you defining routes. Every component of every route should have the properties for the routes (type `RouteComponentProps<PathParams>`), but if you see your component is not having these properties, you can always decorate your component or container (if you use react-redux in a component) with `withRouter()`. The `RouteComponentsProps<PathParams>` define the props mentioned before. But the type `PathParams` is something for you. If your route has parameters, you should define them here. For example, the path `/:page(\\d+)` will have the type `{ page: string }` (yep, `string`).

### react-toastify

Send notifications with [react-toastify][18] is as easy as `toast(<p>YES</p>, Options)`. There's also some predefined styles like success, error, warning, etc., that can be used with `toast.success(<p>Successful</p>)` and so. The toast returns a number that is a unique identifier for the notification. With `toast.update(toastId, Options)` you can modify the notification. That can be useful when showing a loading or updating notification and, with the result, you want to modify the same notification. Remember to read the [documentation][18] to known all the options available.

### SCSS

Never forget how to write good `.scss` files. [SASS/SCSS][23] is an extension to CSS that allows you to simplify and give "_superpowers_" the styling of web pages. In the folder `/styles` of the common package, you can see some files, all of them starting with underscore (_). These are files for SCSS but them itself do not conform a full CSS file. In `/components/app/app.scss` (from the common package) you will find the final CSS file when running the compiler of the project.

SCSS is more like CSS but with the powers of SASS, is a mix of both. All code from CSS works correctly in SCSS but not in SASS. SCSS will be familiar to you if you already know CSS.

```css
.class {
  background-color: lightblue;
  transition: background-color .15s ease-in-out;
}

.class:hover {
  background-color: #BBEDFF;
}

.class > .item {
  color: #333333;
}

.class > .item:last-of-type {
  padding-bottom: 5px;
}
```

```scss
.class {
  background-color: lightblue;
  transition: background-color .15s ease-in-out;

  &:hover {
    background-color: #BBEDFF;
  }

  > .item {
    color: #333333;

    &:last-of-type {
      padding-bottom: 5px;
    }
  }
}
```

Both snippets are the same CSS, but the latter is more compact and shows the relation of the items by making children styles.

You can use `.scss` files if you want, but you can also use `.css` without problem. But if you use CSS files, you must unignore them from the `.gitignore`.

To import the generated `.css` from SCSS or the desired `.css` into the project, simply import them in a file: `import 'src/styles/my-styles.css';`.


## What is the arquitecture of the generated react app

When you run the [Getting started](#tldr) command, you will see the following folder structure:

```
 test-app
   README.md
   node_modules/
   package.json
   public/
     index.html
   src/
     common/
       ... git submodule
     components/
       hello.tsx
       imatge.jpg
     containers/
     redux/
       index.ts
     index.tsx
     registerServiceWorker.ts
     routes.tsx
   .gitignore
   images.d.ts
   tsconfig.json
   tsconfig.prod.json
   tsconfig.test.json
   tslint.json
```

As mentioned in the [README.md][36], the `src` is where all the sources live.

The `src/common` folder is a git module that you must have created when generating the new app. This common folder (mentioned before as common package) contains common code for all the module apps, and it is mandatory to have it, if not, the new module won't work.

Inside `src/components` you should add the [React][1] components you define, including pages. Everything. The interfaces you need to define like `...StateToProps`, `...DispatchToProps`, `...OwnProps` should be available inside this folder or inside a new one, it is up to you.

On `src/containers` should contain only the creation of the containers (the bindings between a component and the redux state).

For defining reducers, actions and interfaces related to both, `src/redux` is your best option.

`routes.tsx` allows you to define the routes available in the app, but in a easy way.

All files from from this package has comments that will help you understand a bit more how everything works. Also, the common package has some documentation that will help you too.

For running the development server, you must have one or more servers that exposes the following paths:

 - `/locales` with the content of the locales repository.
 - `/icons` with the content of the icons repository.
 - `/app-api` with a mock API server or the real API server.

By default, they are all set to proxy the requests to the server `localost:3001`, but you can change them in the `package.json`.

The flow that follows when you run the app is the following:

 1. Load dependencies (not all, remember code splitting) and initialize localization
 2. Check for a valid session or gather it from the query (`window.location.query`)
    1. If it is not valid, redirect to `/` home page
 3. Initialize redux (with the user from the session and its permissions) and load bootstrap
 4. Initialize the app
    1. Configure some components (as seen in the comments of `index.tsx`)
    2. Prepare the pages that the user can see
    3. Prepare the header
    4. Prepare the sidebar, with the modules that can see and the pages of the loaded module that can also see
    5. Prepare the routing system with the routes that the user is able to access, and the fallback __404 error__
    6. Show the matching page (one of your definitions or the _not found_ one).
    7. If it detects an unhandled error (you must use misnamed `catchUnauthorizedAndForbiddenErrors` from `src/common/redux/errors/decorator.ts` in any asynchronous action when using the `http-client`), it will show a toast and clear the error. If the error is of type `401` (unauthorized), it will show a toast and then redirect to the login page. If the error is `5XX`, it will show a notification but nothing more (you must do something with it).

 > **Remember to use** `catchUnauthorizedAndForbiddenErrors` from `src/common/redux/errors/decorator.ts` in any async action:
 >
 > `const someAsyncAction = () => catchUnauthorizedAndForbiddenErrors(async (dispatch: Dispatch) => { ... });`


  [1]: https://reactjs.org/
  [2]: https://www.npmjs.com/package/react-dom
  [3]: https://www.typescriptlang.org
  [4]: http://getbootstrap.com
  [5]: https://jonsuh.com/hamburgers/
  [6]: https://www.i18next.com
  [7]: https://github.com/i18next/i18next-browser-languageDetector
  [8]: https://github.com/i18next/i18next-xhr-backend
  [9]: http://jquery.com
  [10]: http://momentjs.com
  [11]: https://popper.js.org
  [12]: https://github.com/nfl/react-helmet
  [13]: https://react.i18next.com
  [14]: https://github.com/reduxjs/react-redux
  [15]: https://reacttraining.com/react-router/
  [16]: https://reacttraining.com/react-router/web/guides/philosophy
  [17]: https://github.com/drcmda/react-spring
  [18]: https://github.com/fkhadra/react-toastify
  [19]: http://redux.js.org
  [20]: https://github.com/reduxjs/redux-thunk
  [21]: https://www.npmjs.com/package/npm-run-all
  [22]: https://www.npmjs.com/package/node-sass-chokidar
  [23]: http://sass-lang.com
  [24]: https://reactjs.org/tutorial/tutorial.html
  [25]: https://reactjs.org/docs/getting-started.html
  [26]: http://getbootstrap.com/docs/4.1/layout/grid/
  [27]: http://getbootstrap.com/docs/4.1/content/typography/
  [28]: http://getbootstrap.com/docs/4.1/content/tables/
  [29]: https://getbootstrap.com/docs/4.1/utilities/
  [30]: https://redux.js.org/introduction
  [31]: https://redux.js.org/glossary
  [32]: https://redux.js.org/basics
  [33]: https://redux.js.org/advanced
  [34]: https://github.com/reduxjs/react-redux/blob/master/docs/api.md#connectmapstatetoprops-mapdispatchtoprops-mergeprops-options
  [35]: https://reacttraining.com/react-router/web/guides/philosophy
  [36]: https://github.com/wmonk/create-react-app-typescript/blob/master/template/README.md#folder-structure
